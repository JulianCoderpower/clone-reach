app.directive('dropdown', [
    function(){
        return {
            restrict: 'E',
            scope: true,
            link: link
        };

        function link(scope){
            scope.dropdown = {
                open: false
            };

            scope.dropdown.toggleDisplay = function(){
                scope.dropdown.open = !scope.dropdown.open;
            };
        }
}]);