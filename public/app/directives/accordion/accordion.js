app.directive('accordion', [
    function() {
        return {
            restrict: 'E',
            scope: true,
            link: link
        };

        function link(scope){
            scope.accordion = {
                open: false
            };
            scope.accordion.toggleDisplay = function(){
                scope.accordion.open = !scope.accordion.open;
            }
        }
    }]);