app.directive('navigation', [
    function() {
        return {
            restrict: 'E',
            templateUrl: '/public/app/directives/templates/navigation/navigation.html'
        }
    }]);