app.config([
    '$routeProvider',
    function($routeProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'app/pages/auth/auth.html',
            controller: 'AuthController'
        })
        .when('/reporting', {
            templateUrl: 'app/pages/reporting/reporting.html',
            controller: 'ReportingController'
        })
        .when('/contacts', {
            templateUrl: 'app/pages/contact/list/list.html',
            controller: 'ContactController'
        })
        .when('/contacts/:id', {
            templateUrl: 'app/pages/contact/one/one.html',
            controller: 'ContactOneController'
        })
        .otherwise('/');
    }]);
