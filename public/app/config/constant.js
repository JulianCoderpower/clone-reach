app.config(function($provide){
    $provide.constant('PERIODS', {
        TODAY: 'today',
        THIS_WEEK: 'thisweek',
        THIS_MONTH: 'thismonth',
        YESTERDAY: 'yesterday',
        YESTERWEEK: 'yesterweek',
        YESTERMONTH: 'yestermonth'
    });
});