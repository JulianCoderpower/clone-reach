app.service('UserService', [
    '$http',
    function($http) {
        var UserService = {};
        UserService.list = function() {
            return $http.get('http://reach.bemyapp.com:3333/api/users?max=-1&secretPassword=asoidjasodijasodijasdoaspdjaspdojasdp')
            .then(setData, setError);
        };

        function setData(response) {
            return response.data;
        }

        function setError(response) {
            return response.status + ': ' + response.statusText;
        }

        return UserService;
    }]);
