app.service('AuthService', [
    '$http',
    function($http) {
        var AuthService = {};

        AuthService.auth= function() {
            return $http.get('http://reach.bemyapp.com:3333/');
        };

        AuthService.isAuthenticated = function() {
            return $http.get('http://reach.bemyapp.com:3333/google-auth/profile');
        };

        AuthService.login = function() {
            return $http.get('http://reach.bemyapp.com:3333/google-auth/auth');
        };

        return AuthService;
    }]);