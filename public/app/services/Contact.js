app
.service('ContactService', [
    '$http',
    function($http) {
        var ContactService = {};
        ContactService.list = function(params) {
            return $http.get('http://reach.bemyapp.com:3333/reporting/home/contacts?secretPassword=asoidjasodijasodijasdoaspdjaspdojasdp', params)
            .then(setData, setError);
        };

        ContactService.one = function(id, params) {
            return $http
            .get('http://reach.bemyapp.com:3333/reporting/home/contacts/{id}?secretPassword=asoidjasodijasodijasdoaspdjaspdojasdp'.replace('{id}', id), params)
            .then(setData, setError);
        };

        function setData(response) {
            return response.data;
        }

        function setError(response) {
            return response.status + ': ' + response.statusText;
        }

        return ContactService;
    }]);