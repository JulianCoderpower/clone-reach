app.filter('format', [
        function() {
            return function(date, format) {
                return moment(date).format(format);
            };
        }]);
