app.filter('ago', [
        function() {
            return function(date) {
                return moment(date).fromNow();
            };
        }]);
