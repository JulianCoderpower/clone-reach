app.controller('ContactOneController', [
    '$routeParams',
    '$scope',
    'ContactService',
    function($routeParams, $scope, ContactService) {
        var Init = {
            contact: function(params) {
                ContactService
                .one(params)
                .then(this.setContact, notifyError);
            },
            setContact: function(contact) {
                $scope.contact = contact;
            }
        };

        (function initialize() {
            Init.contact($routeParams.id);
        }());

        function notifyError(error) {
            alert(error);
        }

    }]);