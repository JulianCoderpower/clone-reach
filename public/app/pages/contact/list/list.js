app.controller('ContactController', [
    '$routeParams',
    '$scope',
    'ContactService',
    'PERIODS',
    'UserService',
    function($routeParams, $scope, ContactService, PERIODS, UserService) {
        var Init = {
            contacts: function(params) {
                ContactService
                .list(params)
                .then(setContacts, notifyError);

                function setContacts(contacts) {
                    $scope.contacts = contacts;
                }
            },
            users: function() {
                UserService
                .list()
                .then(setUsers, notifyError);

                function setUsers(data) {
                    $scope.users = data._embedded.users;
                }
            }
        };

        (function initialize() {
            Init.contacts({params:$routeParams});
            Init.users();
            $scope.periods = PERIODS;
        }());


        (function setupWatchers() {
            var reportingWatchers = ['period', 'user'];
            $scope.$watchGroup(reportingWatchers, updateContacts);

            function updateContacts(params) {
                var filter = {
                    params : $routeParams
                };
                filter.params.period = params[0];
                filter.params.user = params[1];
                if(params[0] || params[1])
                    Init.contacts(filter);
            }
        }());


        function notifyError(error) {
            alert(error);
        }

    }]);