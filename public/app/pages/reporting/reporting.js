app.controller('ReportingController', [
    '$scope',
    'PERIODS',
    'ReportingService',
    'UserService',
    function($scope, PERIODS, ReportingService, UserService) {
        var Init = {
            reporting: function(params) {
                ReportingService
                .list(params)
                .then(setReporting, notifyError);

                function setReporting(reporting) {
                    $scope.reporting = reporting;
                }
            },
            users: function() {
                UserService
                .list()
                .then(setUsers, notifyError);

                function setUsers(data) {
                    $scope.users = data._embedded.users;
                }
            }
        };

        (function initialize() {
            Init.reporting();
            Init.users();
            $scope.periods = PERIODS;
        }());

        (function setupWatchers() {
            var reportingWatchers = ['period', 'user'];
            $scope.$watchGroup(reportingWatchers, updateReporting);

            function updateReporting(params) {
                var filter = {
                    params: {
                        period: params[0],
                        user: params[1]
                    }
                };
                Init.reporting(filter);
            }
        }());

        function notifyError(error) {
            alert(error);
        }

    }]);
